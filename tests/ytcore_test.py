#!/usr/bin/env python3
import unittest

import ytcore

class YTCoreTest(unittest.TestCase):
    
    def test_searchquery_tagesschau(self):
        searchterm = "tagesschau"
        channel_title = "tagesschau"
        core = ytcore.Core("de", "DE")
        s = core.new_search(searchterm=searchterm)
        results = s.search()
        for item in results:
            if item["type"] == "channel":
                self.assertEqual(item["title"], channel_title)
    
    def test_cached_languagelists_against_latest(self):
        core = ytcore.Core()
        self.assertListEqual(core.get_languagelist_from_self(), core.get_languagelist_from_youtube())
    
    def test_cached_countrylists_against_latest(self):    
        core = ytcore.Core()
        self.assertListEqual(core.get_countrylist_from_self(), core.get_countrylist_from_youtube())

if __name__ == '__main__':
    unittest.main()
