# Documentation:

## Dicts:

### Thumbnail:

{
   "type": "thumbnail",
   "width": 0,
   "height": 0,
   "url": ""
}

### owner

Generally used together within an video page

#### Structure
{
   "type": "owner",
   "Id": "",
   "title": "",
   "thumbnail": list of thumbnails,
   "subscriberCountText": ""
}

### video

Generally found inside a video feed like search-results or recommendations.

#### Structure
{
   "type": "video",
   "Id": "",
   "thumbnail": list of thumbnails,
   "title": "",
   "description": "",
   "viewCountText": "",
   "publishedTimeText": "",
   "lengthText": "",
   "ownerId": "",
   "ownerText": ""
}

### channel

Generally found inside an video feed.
