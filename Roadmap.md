# Roadmap for ytcore

Format: Done | Goal | (further explonation) | (priority)

- [ ] Parse metadata into diffrent datatypes (other than str) (depends on local language) (low)
<!--- [ ] decode the encoded Encoding from youtube (replacing html-symbols) (low)-->
- [ ] Sanitize Urls returned from youtube. (Removing further tracking) (medium)
- [ ] Comments under Videos (high)
- [ ] Using error and errormsg in every class (base class) (low)
- [ ] Tests and Checks, to determine if module is still working (while runtime and perhaps with gitlab ci) (medium)
- [ ] Getting things like YT\_Headers and YT\_SearchContinue\_Payload dynamic if the request fails (high)
- [ ] Feeds (from channel uploads, playlists?, trending) (medium)
- [ ] Channel: Uploads via Feed, Uploads via continue (like SearchContinue), Community, Home, Banner, recommended Channels, user-created playlists .... (medium)
- [ ] \_helper: Implementing more renderers (eg: shelf-renderer)

