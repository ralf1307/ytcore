#    This file is part of ytcore.
#
#    ytcore is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ytcore is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with ytcore.  If not, see <http://www.gnu.org/licenses/>.
#

from .PageClass import Page

class ChannelPage(Page):
    """Frontend for a Page of a channel

    This Page holds thing like channel name, description, banner and requires therefore a banner.
    """

    _backend = None
    
    _success = False
    _continue_success = False

    _channel_id = str()
    
    _channel_banner = []
    _channel_description = str()

