from .DataClass import Data

class Thumbnail(Data):
    Type = "thumbnail"

    Width = 0
    Height = 0

    URL = str()
