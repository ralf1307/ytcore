from .MediaWithOwnerClass import MediaWithOwner

class Video(MediaWithOwner):
    Type = "video"

    Description = ""
    ViewCountText = ""
    PublishedTimeText = ""
    LengthText = ""
