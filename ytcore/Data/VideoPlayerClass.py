from .DataClass import Data

class VideoPlayer(Data):
    Type = "videoplayer"

    Id = "" # video id

    OK = False
    Status = "" # if status == "OK": OK = True

    Videos = [] # FIXME: currently holds unformatted dicts from youtube

    Captions = None # FIXME: currently no datatype
