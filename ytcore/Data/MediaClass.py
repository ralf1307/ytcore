from .DataClass import Data

class Media(Data):
    Type = "media"

    Id = str()
    Title = str()
    Thumbnails = []
