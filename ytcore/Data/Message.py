from .DataClass import Data

class Message(Data):
    Type = "Message"
    
    # Levels: ""info", "warning", "error", "exception"
    #   Info:
    #   Warning: Should be thrown if an unknown key is found
    #   Error: Something interferes with the program
    #   Exception: Error in the python code that got captured via try
    Level = "info"
    # Part: "download", "parse", "credentials", "user"
    Part = ""
    Message = ""
    # Data stores Exception Object if Exception got captured
    Data = None # Can be anything 

    def __init__(self, Level=str(), Part=str, Message=str(), Data=None):
        self.Level = Level
        self.Part = Part
        self.Message = Message
        self.Data = Data


    @staticmethod
    def info(Part=str(), Message=str(), Data=None):
        return Message(Level="info", Part=Part, Message=Message, Data=Data)

    @staticmethod
    def warning(Part=str(), Message=str(), Data=None):
        return Message(Level="warning", Part=Part, Message=Message, Data=Data)

    @staticmethod
    def error(Part=str(), Message=str(), Data=None):
        return Message(Level="error", Part=Part, Message=Message, Data=Data)

    @staticmethod
    def exception(Part=str(), Message=str(), Data=None):
        return Message(Level="exception", Part=Part, Message=Message, Data=Data) 
