from .OwnerClass import Owner

class Channel(Owner):
    """The Channel class is a ownerobject with more information"""
    Type = "channel"
    VideoCountText = ""
    DescriptionSnippet = "" # can be empty
