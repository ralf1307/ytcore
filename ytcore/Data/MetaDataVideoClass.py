from collections.abc import Iterable
from .MetaDataClass import MetaData
from .OwnerClass import Owner as ClassOwner

class MetaDataVideo(MetaData):
    MetaType = "video"

    Title:str = None
    def hasTitle(self) -> bool:
        return isinstance(self.Title, str)

    LikeToDislikePercentage:str = None
    def hasLikeToDislikePercentage(self) -> bool:
        return isinstance(self.LikeToDislikePercentage, int)

    LikeDislikeText:str = None
    def hasLikeDislikeText(self) -> bool:
        return isinstance(self.LikeDislikeText, str)

    ViewCountText:str = None
    def hasViewCountText(self) -> bool:
        return isinstance(self.ViewCountText, str)

    Tags:Iterable[str] = []
    def hasTags(self) -> bool:
        return len(self.Tags) > 0

    DateText:str = None
    def hasDateText(self) -> bool:
        return isinstance(self.DateText, str)

    Description:str = None
    def hasDescription(self) -> bool:
        return isinstance(self.Description, str)

    Owner:ClassOwner = None
    def hasOwner(self) -> bool:
        return isinstance(self.Owner, ClassOwner)
