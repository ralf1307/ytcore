from ytcore.Data import VideoPlayer, MetaDataVideo
from ytcore.Backends.BackendStub import BackendStubVideo

from . import utils as helper

class BackendPossumVideo(BackendStubVideo):
    parent = None
    # some links to parent class
    api = None

    def __init__(self, parent):
        super().__init__(parent)
        self.api = self.parent.api

    def get(self, caller):
        def parse_player(player_raw):
            caller._player = VideoPlayer()
            caller._player.Id = caller._video_id

            player_status = player_raw["playabilityStatus"]
            caller._player.Status = player_status["status"]
            if player_status["status"] == "OK":
                player_response = player_raw["streamingData"]
                if "formats" in player_response:
                    # FIXME: Parse those with helper functions
                    video_formats_adaptive = player_response["adaptiveFormats"]
                    video_formats_static = player_response["formats"]
                    if len(video_formats_static) > 0:
                        caller._player.OK = True
                        # FIXME: we need to parse this
                        caller._player.Videos = video_formats_static

        def parse_recommended(recommended_raw):
            results = recommended_raw["results"]
            caller._recommended, info = helper.parse_content(results)
            for i in info:
                if i["type"] == "autoplay":
                    caller._autoplay = True
                    caller._autoplay_data = i["autoplay"]
                elif i["type"] == "continuation":
                    caller._recommended_continuable = True
                    caller._internal_data["recommended_token"] = i["token"]

        def parse_comment(data):
            if "continuations" in data:
                for item in data["continuations"]:
                    if "nextContinuationData" in item:
                        caller._internal_data["comments_token"] = item["nextContinuationData"]["continuation"]

        def parse_metadata_primary(data):
            m = caller._metadata
            try:    
                m.Title =  helper.getText(data["title"])
            except:
                pass
            # Legacy: viewCountText is depreacted by youtube. remove me pls in a few weeks
            try:
                m.ViewCountText = data["viewCount"]["videoViewCountRenderer"]["viewCount"]["simpleText"]
            except:
                try:
                    m.ViewCountText = helper.videoViewCountRenderer(data["viewCount"])
                except:
                    pass
            try:
                m.LikePercentage = data["sentimentBar"]["sentimentBarRenderer"]["percentIfIndifferent"]
                m.LikeDislikeText = data["sentimentBar"]["sentimentBarRenderer"]["tooltip"]
            except:
                pass
            try:    
                for item in data["superTitleLink"]["runs"]:
                    if item["text"] != " ":
                        m.HashTags.append(item["text"])
            except:
                pass
            try:
                m.DateText = helper.getText(data["dateText"])
            except:
                pass

        def parse_metadata_secondary(data):
            caller._metadata.Description = helper.getText(data["description"], navpoint_enable=False)
            caller._metadata.Owner = helper.ownerRenderer(data["owner"])

        caller._internal_data = {}
        caller._metadata = MetaDataVideo()
        request_raw = self.api.video(caller._video_id)
        for i in request_raw:
            if "preconnect" in i:
                pass  # FIXME: Most likly contains URLs to check for connection
                # (needed if user has bad networking)
            elif "playerResponse" in i:
                parse_player(i["playerResponse"])
            elif "timing" in i:
                pass  # Could be interesting (debug output?)
            elif "response" in i:
                contents = i["response"]["contents"]["twoColumnWatchNextResults"]
                session_token = i["xsrf_token"]
                if "results" in contents:  # Metadata like Video Title...
                    # if this exists, we assume everything is correct
                    caller._success = True
                    # Will break if youtube changes something
                    # Buts its faster.
                    # If we have a dynamic parser for YT_Headers FIXME
                    metadata_raw = contents["results"]["results"]["contents"]
                    for item in metadata_raw:
                        if "videoPrimaryInfoRenderer" in item:
                            parse_metadata_primary(item["videoPrimaryInfoRenderer"])
                        if "videoSecondaryInfoRenderer" in item:
                            parse_metadata_secondary(item["videoSecondaryInfoRenderer"])
                        elif "merchandiseShelfRenderer" in item:
                            pass
                        elif "itemSectionRenderer" in item:
                            parse_comment(item["itemSectionRenderer"])
                if "secondaryResults" in contents:  # What to watch next
                    parse_recommended(
                        contents["secondaryResults"]["secondaryResults"])
                
                pass  # needs further parsing in __parseRecommended and __parseMetadata
            else:
                pass  # Unknown keys, # FIXME: log if this case occurs


    def get_recommended_continue(self, caller):
        caller._recommended_continuable = False
        parsed, info, mode = helper.parse_continue_response(caller._internal_data["recommended_token"])
        if mode == "append":
            self._recommended.append(parsed)
        for i in info:
            if i["type"] == "continuation":
                self._recommended_continuable = True
                caller._internal_data["recommended_token"] = i["token"]
        return parsed
