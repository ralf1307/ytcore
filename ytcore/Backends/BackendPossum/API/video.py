#
#    This file is part of ytcore.
#
#    ytcore is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ytcore is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with ytcore.  If not, see <http://www.gnu.org/licenses/>.
#



import json


def _video(self, Id):
    try:
        checkId(self, Id)
        raw_json = getRawResponse(self, Id)
        parsed_json = parseInital(self, raw_json)
    except:
        raise
    return parsed_json


def checkId(self, Id):
    return True


def getRawResponse(self, Id):
    video_parameters = self.YT_Watch_Parameters.copy()
    video_parameters["v"] = Id
    return self.downloader.get(url=self.YT_Watch_URL, parameters=video_parameters, headers=self.YT_Headers)


def parseInital(self, raw_json):
    # FIXME: implement checks if valid response (if not throw error)
    result = json.loads(raw_json)
    return result
