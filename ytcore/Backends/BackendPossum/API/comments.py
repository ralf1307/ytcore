#
#    This file is part of ytcore.
#
#    ytcore is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ytcore is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with ytcore.  If not, see <http://www.gnu.org/licenses/>.
#

#   https://www.youtube.com/comment_service_ajax?action_get_comments=1&pbj=1&ctoken=EkMSC2JuVGQ2aTVrSzRVMgDIAQDgAQGiAg0o____________AUAAwgIbGhdodHRwczovL3d3dy55b3V0dWJlLmNvbSIAGAY%3D&continuation=EkMSC2JuVGQ2aTVrSzRVMgDIAQDgAQGiAg0o____________AUAAwgIbGhdodHRwczovL3d3dy55b3V0dWJlLmNvbSIAGAY%3D&itct=CJEBEMm3AiITCNeXs_eUuu0CFQjaVQod940Dsg==

import json

def _comments(self, token):
    try:
        raw_json = _get_raw(self, token)
        continue_json = _parse_continue(self, raw_json)
    except:
        raise
    return continue_json


def _get_raw(self, token):
    parameters = self.YT_Comments_Parameters.copy()
    parameters["ctoken"] = token
    # Sets mobile header for mobile url
    headers = self.YT_Headers.copy()
    headers['x-youtube-client-name'] = "2"
    return self.dl.post(url=self.YT_Comments_URL, parameters=parameters, cookies=self.YT_Comments_Cookies, headers=headers)

def _parse_continue(self, raw_json):
    # FIXME : implement checks if valid response (if not throw error)
    result = json.loads(raw_json)
    return result
