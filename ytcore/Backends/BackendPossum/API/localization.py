
#
#    This file is part of ytcore.
#
#    ytcore is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ytcore is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with ytcore.  If not, see <http://www.gnu.org/licenses/>.
#


import json


def _get_languages(self):
    try:
        # Including the youtube headers could reduce tracking potential?
        raw_json = self.dl.get(url=self.YT_Picker_Ajax_URL,
                            parameters=self.YT_Picker_Ajax_Language, headers=self.YT_Headers)
        return json.loads(raw_json)
    except:
        raise


def _get_countries(self):
    try:
        raw_json = self.dl.get(url=self.YT_Picker_Ajax_URL,
                            parameters=self.YT_Picker_Ajax_Country, headers=self.YT_Headers)
        return json.loads(raw_json)
    except:
        raise
