from .BackendStub import BackendStub, BackendStubSearch, BackendStubVideo


def get_list():
    backends = []
    backends.append(BackendStub)
    try:
        from .BackendPossum import BackendPossum
        backends.append(BackendPossum)
    except:
        pass
    return backends
