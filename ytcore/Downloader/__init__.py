from .StubDownloader import StubDownloader

def get_list() -> list:
    dls = [StubDownloader]
    try:
        from .SimpleDownloader import SimpleDownloader
        dls.append(SimpleDownloader)
    except:
        pass
    return dls
