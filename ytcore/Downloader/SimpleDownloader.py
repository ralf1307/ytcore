#
#    This file is part of ytcore.
#
#    ytcore is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ytcore is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with ytcore.  If not, see <http://www.gnu.org/licenses/>.
#

from ytcore.Downloader.StubDownloader import StubDownloader

#FIXME: Add documentaion for SimpleDownloader
class SimpleDownloader(StubDownloader):
    import requests
    # (i want to replace Requests later with urllib3 (in a long future), as i want to reduce the dependencies)

    def get(self, url=str(), parameters=dict(), data=str(), cookies=dict(), headers=dict()):
        # example: https://example.org/someurl?parameterone=1234  HTTP-Header
        #          ----------url-------------- ---parameters----  --headers--
        request = self.requests.get(url, cookies=cookies, params=parameters, headers=headers)
        request.raise_for_status()  # Raises Expetion if not OK
        return request.text

    def post(self, url=str(), parameters=dict(), data=str(), cookies=dict(), headers=dict()):
        request = self.requests.post(url, cookies=cookies, data=data, params=parameters, headers=headers)
        request.raise_for_status()
        return request.text
