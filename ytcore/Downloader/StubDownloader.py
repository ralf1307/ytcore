#
#    This file is part of yt_core.
#
#    yt_core is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    yt_core is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with yt_core.  If not, see <http://www.gnu.org/licenses/>.
#

#FIXME: Add documentaion for StubDownloader
class StubDownloader:
    def get(self, url=str(), parameters=dict(), data=str(), cookies=dict(), headers=dict()):
        # example: https://example.org/someurl?parameterone=1234  HTTP-Header
        #          ----------url-------------- ---parameters----  --headers--
        #FIXME: Proper Error Handling: Stub Method Exception
        return ""

    def post(self, url=str(), parameters=dict(), data=str(), cookies=dict(), headers=dict()):
        #FIXME: Proper Error Handling: Stub Method Exception
        return ""
