#    This file is part of ytcore.
#
#    ytcore is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ytcore is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with ytcore.  If not, see <http://www.gnu.org/licenses/>.
#

from .PageClass import Page

#FIXME: Document VideoPage methods
class VideoPage(Page):
    """Frontend for a VideoPage

    This Page deals with a singular video and requires therefore a video_id.


    Note
    ----
    Initalizing pages manually is not supported and
    will break in future version due to internal API-changes.
    Please use ytcore.Core.new_video for that. Thanks :)
    """

    _backend = None


    _success = False
    _continue_success = False

    # Info:
    _autoplay_available = False
    _recommended_continuable = False
    _error = []

    # Data:
    _video_id = "" # youtube can redirect videos (happens with music vids)
    _metadata = None
    _autoplay_data = None # has a "type": "video"
    _recommended = []
    _player = None

    comments = None # object of class Comments

    # Used for storage like tokens by the backend.
    #   (the backends shouldnt store to specific data)
    _internal_data = None

    # Logic Methods
    def __init__(self, core, video_id: str):
        self.backend = core.get_backend().video
        self._video_id = video_id

    def init(self):
        self.backend.get(self)
        return self._success

    def recommended_continuation(self) -> bool:
        if self._recommended_continuable:
            self.backend.get_recommended_continue(self)
        return self._continue_success

    # Get: Boolean Methods
    def is_recommended_available(self):
        if len(self._recommended) == 0:
            return False
        return True

    def is_recommended_continuable(self):
        return self._recommended_continuable

    def is_autoplay_available(self):
        return self._autoplay_available

    # Get: returns data
    def get(self):
        return self._player

    def get_player(self):
        return self._player

    def get_metadata(self):
        return self._metadata
    
    def get_recommended(self):
        return self._recommended

    # Static Methods
    @staticmethod
    def check_id(video_id):
        #FIXME: Currently defaults to True, implement the check
        return True
