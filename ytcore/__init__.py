"""youtube extractor in python3.

This module should ease the development of inofficial youtube apps.
The main class you are going to use is ytcore.Core, as it stores the
most important data and is your interface to this module.

Use ytcore.Core()
------------------
Core() stores localization data and manages the available backends and downloaders.
You can select the backends and downloaders with the constructor or change them at runtime.
(Note: Backend and Downloader-Changes wont effect already created pages.
If it did, its behaviour would be undefined!)
From Core() you can initialize Pages like VideoPage() or SearchPage().

    # Query available backends with this version of ytcore
    $ backends = Core.get_backends_available()
    # Same thing with downloads
    $ downloaders = Core.get_downloaders_available()
    # Choose your backend and downloader
    # Initalizes a new Core()
    $ core = Core(backend=BackendPossum downloader=SimpleDownloader)
    # Initalize a new video page via Core() with the video_id="rRPQs_kM_nw"
    $ video = core.new_video("rRPQs_kM_nw")
    # Fetch the data from a backend!
    $ video.init()

"""
#    This file is part of ytcore.
#
#    ytcore is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ytcore is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with ytcore.  If not, see <http://www.gnu.org/licenses/>.
#

from . import Backends
from . import Downloader
from .SearchPageClass import SearchPage
from .VideoPageClass import VideoPage

# Dev Functions for easier testing:
# PLS DONT USE THESE IN PRODUCTION, AS THEY ARE GOING TO BE REMOVED
def dev_video(video_id="dQw4w9WgXcQ"):
    from .Downloader.SimpleDownloader import SimpleDownloader
    from .Backends.BackendPossum import BackendPossum
    core = Core(downloader=SimpleDownloader, backend=BackendPossum)
    vid = core.new_video(video_id)
    print(f"Video by id: {video_id}, success: {vid.init()}")
    return vid

def dev_search(searchquery="Never gonna give you up"):
    from .Downloader.SimpleDownloader import SimpleDownloader
    from .Backends.BackendPossum import BackendPossum
    core = Core(downloader=SimpleDownloader, backend=BackendPossum)
    search = core.new_search(searchquery)
    print(f"Search success: {search.init()}")
    print(f"SearchQuery: {searchquery}")
    return search
# End Dev Functions

# Main Class:
def get_backends_available():
    return Backends.get_list()
def get_downloaders_available():
    return Downloader.get_list()

class Core:
    """ytcore.Core is needed for all the other classes in this library.
    This base class includes all the basic info the other classes need like Localization.
    """
    _dl = None
    _backend = None

    def __init__(self, language="de", country="DE",
            backend=Backends.BackendStub, downloader=Downloader.StubDownloader):
        """Constructor of ytcore.Core()
        You can optionally specify the first backend to use (Defaults to BackendStub)
        and the downloader (Defaults to Downloader.Stub).

        Otherwise you need to update them later with update_backend(backend_class) and
        update_downloader(downloader_class)
        """
        self._dl = downloader()
        self._backend = backend(self)
        self.update_localization(language, country)

    def new_search(self, searchquery:str):
        """Returns a search object with the given searchquery.
        Use help(ytcore.SearchPage)!"""
        return SearchPage(self, searchquery=searchquery)

    def new_video(self, video_id:str):
        """Returns a video object with the given video_id.
        Use help(ytcore.VideoPage)!"""
        return VideoPage(self, video_id)

    @staticmethod
    def get_backends_available():
        """List the available Backends from ytcore.
        Later versions may have more backends and the user may
        desire to use them."""
        return Backends.get_list()

    def get_backend(self):
        """Returns the backend the user has chosen."""
        return self._backend

    def update_backend(self, backend=None) -> bool:
        """Sets a new backend.
        Returns True on success!
        """
        # FIXME: check if valid backend
        self._backend = backend(self)
        return True

    @staticmethod
    def get_downloaders_available():
        """List the available Downloaders ytcore can use.
        Later versions may have more downloaders and the user may
        desire to use them"""
        return Downloader.get_list()

    def get_downloader(self):
        "Returns the chosen downloader object."
        return self._dl

    def update_downloader(self, downloader=None):
        """Returns True on success!"""
        # FIXME: check if valid downloader
        self._dl = downloader(self)
        return self._dl

    def get_countrylist_from_self(self):
        """Returns the cached country list in this object."""
        return self._country_list

    def get_languagelist_from_self(self):
        """Returns the language list in this object."""
        return self._language_list

    def get_current_language(self):
        """Returns the currently selected language.
        Returns [language_code:str, language_name:str]"""
        return self._language_current

    def get_current_country(self):
        """Returns the currently selected country.
        Returns [country_code:str, country_name:str, preferred_localization_for_country:str]"""
        return self._country_current

    def update_localization(self, language, country):
        """Sets the current language and country.
        Accepts both language's names and codes. Same for countries.
        If the language or country isn't found in the local cache it will
        try to download the current list from your backend and setting it again"""
        self._update_language(language)
        self._update_country(country)
        self._backend.update_localization(self)

    def update_language(self, lang):
        """Sets the current languag
        Accepts both the name and the code as input.
        If the language isn't found in the local cache it will try to
        download the current list from your backend and setting it again."""
        self._update_language(lang)
        self._backend.update_localization(self)

    def _update_language(self, lang, first_run=True):
        found = False
        for langs in self._language_list:
            for i in langs:
                if lang.lower() == i.lower():
                    found = True
                    self._language_current = langs
        if found:
            pass
        elif first_run:
            self._language_list = self._backend.get_languagelist()
            self._update_language(lang, first_run=False)
        else:
            pass  #FIXME: CREATE CUSTOM ERROR EXPECTION

    def update_country(self, country):
        """Sets the current country.
        Accepts both the name and the code as input.
        If the country isn't found in the local cache it will try
        to download the current list from youtube and setting it again."""
        self._update_country(country)
        self._backend.update_localization(self)

    def _update_country(self, country, first_run=True):
        found = False
        for i in self._country_list:
            for coun in i:
                if country.lower() == coun.lower():
                    found = True
                    self._country_current = i
        if found:
            pass
        elif first_run:
            self._country_list = self._backend.get_countrylist()
            self._update_country(country, first_run=False)
        else:
            pass  #FIXME: CREATE CUSTOM ERROR EXPECTION

    #def _update_header(self):
    #    self._dl.YT_Headers["Accept-Language"] = self._language_current[0] + \
    #       "-" + self._country_current[0]

    _language_current = ["de", "deutsch"]
    _country_current = ["DE", "Deutschland"]

    # Retrieved at 4.12.2020.
    _language_list = [['af', 'Afrikaans'], ['az', 'Azərbaycan'], ['id', 'Bahasa Indonesia'], ['ms', 'Bahasa Malaysia'], ['bs', 'Bosanski'], ['ca', 'Català'], ['cs', 'Čeština'], ['da', 'Dansk'], ['de', 'Deutsch'], ['et', 'Eesti'], ['en-IN', 'English (India)'], ['en-GB', 'English (UK)'], ['en', 'English (US)'], ['es', 'Español (España)'], ['es-419', 'Español (Latinoamérica)'], ['es-US', 'Español (US)'], ['eu', 'Euskara'], ['fil', 'Filipino'], ['fr', 'Français'], ['fr-CA', 'Français (Canada)'], ['gl', 'Galego'], ['hr', 'Hrvatski'], ['zu', 'IsiZulu'], ['is', 'Íslenska'], ['it', 'Italiano'], ['sw', 'Kiswahili'], ['lv', 'Latviešu valoda'], ['lt', 'Lietuvių'], ['hu', 'Magyar'], ['nl', 'Nederlands'], ['no', 'Norsk'], ['uz', 'O‘zbek'], ['pl', 'Polski'], ['pt-PT', 'Português'], ['pt', 'Português (Brasil)'], ['ro', 'Română'], ['sq', 'Shqip'], ['sk', 'Slovenčina'], ['sl', 'Slovenščina'], ['sr-Latn', 'Srpski'], ['fi', 'Suomi'], ['sv', 'Svenska'], ['vi', 'Tiếng Việt'], ['tr', 'Türkçe'], ['be', 'Беларуская'], ['bg', 'Български'], ['ky', 'Кыргызча'], ['kk', 'Қазақ Тілі'], ['mk', 'Македонски'], ['mn', 'Монгол'], ['ru', 'Русский'], ['sr', 'Српски'], ['uk', 'Українська'], ['el', 'Ελληνικά'], ['hy', 'Հայերեն'], ['iw', 'עברית'], ['ur', 'اردو'], ['ar', 'العربية'], ['fa', 'فارسی'], ['ne', 'नेपाली'], ['mr', 'मराठी'], ['hi', 'हिन्दी'], ['as', 'অসমীয়া'], ['bn', 'বাংলা'], ['pa', 'ਪੰਜਾਬੀ'], ['gu', 'ગુજરાતી'], ['or', 'ଓଡ଼ିଆ'], ['ta', 'தமிழ்'], ['te', 'తెలుగు'], ['kn', 'ಕನ್ನಡ'], ['ml', 'മലയാളം'], ['si', 'සිංහල'], ['th', 'ภาษาไทย'], ['lo', 'ລາວ'], ['my', 'ဗမာ'], ['ka', 'ქართული'], ['am', 'አማርኛ'], ['km', 'ខ្មែរ'], ['zh-CN', '中文 (简体)'], ['zh-TW', '中文 (繁體)'], ['zh-HK', '中文 (香港)'], ['ja', '日本語'], ['ko', '한국어']]  # Ignore PEP8Bear
    _country_list = [['EG', 'Ägypten', 'ar_EG'], ['DZ', 'Algerien', 'ar_DZ'], ['AR', 'Argentinien', 'es_AR'], ['AZ', 'Aserbaidschan', 'az_AZ'], ['AU', 'Australien', 'en_AU'], ['BH', 'Bahrain', 'ar_BH'], ['BD', 'Bangladesch', 'bn_BD'], ['BE', 'Belgien', 'en_BE'], ['BO', 'Bolivien', 'es_BO'], ['BA', 'Bosnien und Herzegowina', 'bs_BA'], ['BR', 'Brasilien', 'pt_BR'], ['BG', 'Bulgarien', 'bg_BG'], ['CL', 'Chile', 'es_CL'], ['CR', 'Costa Rica', 'es_CR'], ['DK', 'Dänemark', 'da_DK'], ['DE', 'Deutschland', 'de_DE'], ['DO', 'Dominikanische Republik', 'es_DO'], ['EC', 'Ecuador', 'es_EC'], ['SV', 'El Salvador', 'es_SV'], ['EE', 'Estland', 'et_EE'], ['FI', 'Finnland', 'fi_FI'], ['FR', 'Frankreich', 'fr_FR'], ['GE', 'Georgien', 'ka_GE'], ['GH', 'Ghana', 'en_GH'], ['GR', 'Griechenland', 'el_GR'], ['GT', 'Guatemala', 'es_GT'], ['HN', 'Honduras', 'es_HN'], ['HK', 'Hongkong', 'zh_HK'], ['IN', 'Indien', 'en_IN'], ['ID', 'Indonesien', 'id_ID'], ['IQ', 'Irak', 'ar_IQ'], ['IE', 'Irland', 'en_IE'], ['IS', 'Island', 'is_IS'], ['IL', 'Israel', 'en_IL'], ['IT', 'Italien', 'it_IT'], ['JM', 'Jamaika', 'en_JM'], ['JP', 'Japan', 'ja_JP'], ['YE', 'Jemen', 'ar_YE'], ['JO', 'Jordanien', 'ar_JO'], ['CA', 'Kanada', 'en_CA'], ['KZ', 'Kasachstan', 'kk_KZ'], ['QA', 'Katar', 'ar_QA'], ['KE', 'Kenia', 'en_KE'], ['CO', 'Kolumbien', 'es_CO'], ['HR', 'Kroatien', 'hr_HR'], ['KW', 'Kuwait', 'ar_KW'], ['LV', 'Lettland', 'lv_LV'], ['LB', 'Libanon', 'ar_LB'], ['LY', 'Libyen', 'ar_LY'], ['LI', 'Liechtenstein', 'zxx_LI'], ['LT', 'Litauen', 'lt_LT'], ['LU', 'Luxemburg', 'lb_LU'], ['MY', 'Malaysia', 'ms_MY'], ['MT', 'Malta', 'en_MT'], ['MA', 'Marokko', 'ar_MA'], ['MX', 'Mexiko', 'es_MX'], ['ME', 'Montenegro', 'sr_ME'], ['NP', 'Nepal', 'ne_NP'], ['NZ', 'Neuseeland', 'en_NZ'], ['NI', 'Nicaragua', 'es_NI'], ['NL', 'Niederlande', 'nl_NL'], ['NG', 'Nigeria', 'en_NG'], ['MK', 'Nordmazedonien', 'mk_MK'], ['NO', 'Norwegen', 'nb_NO'], ['OM', 'Oman', 'ar_OM'], ['AT', 'Österreich', 'de_AT'], ['PK', 'Pakistan', 'ur_PK'], ['PA', 'Panama', 'es_PA'], ['PG', 'Papua-Neuguinea', 'zxx_PG'], ['PY', 'Paraguay', 'es_PY'], ['PE', 'Peru', 'es_PE'], ['PH', 'Philippinen', 'fil_PH'], ['PL', 'Polen', 'pl_PL'], ['PT', 'Portugal', 'pt_PT'], ['PR', 'Puerto Rico', 'es_PR'], ['RO', 'Rumänien', 'ro_RO'], ['RU', 'Russland', 'ru_RU'], ['SA', 'Saudi-Arabien', 'ar_SA'], ['SE', 'Schweden', 'sv_SE'], ['CH', 'Schweiz', 'de_CH'], ['SN', 'Senegal', 'fr_SN'], ['RS', 'Serbien', 'sr_RS'], ['ZW', 'Simbabwe', 'en_ZW'], ['SG', 'Singapur', 'en_SG'], ['SK', 'Slowakei', 'sk_SK'], ['SI', 'Slowenien', 'sl_SI'], ['ES', 'Spanien', 'es_ES'], ['LK', 'Sri Lanka', 'si_LK'], ['ZA', 'Südafrika', 'en_ZA'], ['KR', 'Südkorea', 'ko_KR'], ['TW', 'Taiwan', 'zh_TW'], ['TZ', 'Tansania', 'sw_TZ'], ['TH', 'Thailand', 'th_TH'], ['CZ', 'Tschechien', 'cs_CZ'], ['TN', 'Tunesien', 'ar_TN'], ['TR', 'Türkei', 'tr_TR'], ['UG', 'Uganda', 'en_UG'], ['UA', 'Ukraine', 'uk_UA'], ['HU', 'Ungarn', 'hu_HU'], ['UY', 'Uruguay', 'es_UY'], ['US', 'USA', 'en_US'], ['VE', 'Venezuela', 'es_VE'], ['AE', 'Vereinigte Arabische Emirate', 'ar_AE'], ['GB', 'Vereinigtes Königreich', 'en_GB'], ['VN', 'Vietnam', 'vi_VN'], ['BY', 'Weißrussland', 'be_BY'], ['CY', 'Zypern', 'zxx_CY']]
