# [YTCore](https://git.sr.ht/~ralf1307/ytcore) - an inoffical youtube api parser

[[_TOC_]]

# WARNING

Please wait till v1.0.0 for a stable API I'm going to support for a longer period.

## Introduction

youtube-dl is way too complicated and doesnt offer the right API for proper apps.

One example is the youtube-dl search API. You're only allowed to search for a specified amount of results.

That doesnt work for feature-complete apps. (endless scrolling)

youtube-dl is good for one thing: Getting the video from Youtube.


This is where ytcore comes in:

ytcore is a python library that should feel like using youtube.

### Example:

| Website                                       | yt\_core                               |
|-----------------------------------------------|----------------------------------------|
| Going to youtube.com                          | core = ytcore.Core()                   |
| Using the searchbar                           | search = core.new_search("Rick Astley")|
| Pressing the searchbutton                     | search.init()                          |
| Scroll through the page and see all results   | search.get_results()                   |

## Apps/Frontends using ytcore

[CoreViewer](https://git.sr.ht/~ralf1307/CoreViewer)

## Current dependencies

- [requests](https://requests.readthedocs.io/), and its dependencies 
- Python 3.8.6 \(should work on way more, currently the only version tested)

This module is being written and currently tested on Alpine-Linux/pmOS \(PineBookPro arm64\),
but it should also work on other operating systems and architectures (It's python3 :D).

## Motivation

Allowing apps like NewPipe for Android, but for the PinePhone + phosh.
And they have to be FOSS! \(That's the reason for the [AGPLv3 license](LICENSE).\)
In the future other platforms perhaps, as I dont see Java as a good choice for modern GUIs or Apps. 
This project was dead in 2021 as I (@ralf1307) discovered Invidious. It's a lot of trouble for me to self host it on master as they dont tag releases and I dont like the obscure language.

## How to use

- Make sure you have the needed dependencies (get them via `pip`, or let `setup.py` handle them)
- Get this project (git clone, downloading the tarball, floppies...)
- Install it via `setup.py` (or adjust your python3-path to the root of the repo.)
- Open python3 and `import ytcore`
- `help(ytcore)` or view doc-pages if available

